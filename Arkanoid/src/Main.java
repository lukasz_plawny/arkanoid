import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * @author Lukasz Plawny A <code>Main</code> class creates and starts GUI
 */

public class Main{
	private final static Dimension SCREEN_SIZE;

	static {
		Toolkit tool = Toolkit.getDefaultToolkit();
		SCREEN_SIZE = tool.getScreenSize();
	}

	public static void main(String[] argsv) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new JFrame("ARKANOID");
				frame.setLayout(new BorderLayout());
				frame.setSize((int) SCREEN_SIZE.getWidth() / 3,
						(int) SCREEN_SIZE.getHeight() / 2);
				frame.setResizable(false);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setLocationByPlatform(true);

				frame.add(new PlayArea(frame), BorderLayout.CENTER);
				Image img = new ImageIcon(this.getClass().getResource("/drawable/red_ball.png")).getImage();
				frame.setIconImage(img);
				frame.setFocusable(true);
				frame.requestFocus();
				frame.setVisible(true);

			}
		});
	}

	/**
	 * @return width of the game's frame
	 */
	public static int getFrameWidth() {
		return (int) SCREEN_SIZE.getWidth() / 3;
	}

	/**
	 * @return height of the game's frame
	 */
	public static int getFrameHeight() {
		return (int) SCREEN_SIZE.getHeight() / 2;
	}
}

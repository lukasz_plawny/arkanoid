import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JComponent;

public class Ball{
	private Rectangle rectangle = new Rectangle(); // kwadrat, w kt�ry wpisana jest kula
	private Dimension vectorMove = new Dimension(); // vector ruchu kuli
	private int radius = (int) Main.getFrameWidth() / 50; // promie� kuli
	private final PlayArea PLAY_AREA;
	private int afterHitCounter = 0; // zmienna do odliczania czasu
										// pod�wietlania platformy na czerwono
	private static boolean isBallHitedByPlatform = false;
	private Rectangle hited; // uderzony blok

	public Ball(PlayArea playArea) {
		reset();
		this.PLAY_AREA = playArea;
	}

	public void reset() {
		rectangle.setRect((int) (Main.getFrameWidth() / 2 - radius),
				(int) Main.getFrameHeight() * 2 / 3, 2 * radius, 2 * radius);
	}

	public void setVectorMove(int movementX, int movementY) {
		vectorMove.setSize(movementX, movementY);
	}

	public void moveBall() {
		if (rectangle.y + 2 * radius > PLAY_AREA.getHeight()) // uderzenie w d�
																// planszy-koniec
																// gry
			PLAY_AREA.endGame();
		else {
			if ((rectangle.x + 2 * radius >= PLAY_AREA.getWidth() && vectorMove.width > 0)
					|| rectangle.x <= 0 && vectorMove.width < 0) // uderzenie w
																	// prawy lub
																	// lewy bok
																	// planszy
				vectorMove.width = -vectorMove.width;
			if (rectangle.y <= 0 && vectorMove.height < 0) // uderzenie w g�rny
															// bok
															// planszy
				vectorMove.height = -vectorMove.height;

			rectangle.setLocation(rectangle.x + vectorMove.width, rectangle.y
					+ vectorMove.height);
			if (ballHitPlatform()) { // odbicie od platformy
				if (rectangle.y + 2 * radius == PLAY_AREA.getPlatform()
						.getLocationPoint().y
						&& rectangle.x + 2 * radius >= PLAY_AREA.getPlatform()
								.getLocationPoint().x
						&& rectangle.x <= PLAY_AREA.getPlatform()
								.getLocationPoint().x
								+ PLAY_AREA.getPlatform().getWidth()) { // odbicie
																		// od
																		// g�rnej
																		// powierzchni
																		// platformy
					vectorMove.height = -vectorMove.height;
				} else {
					if ((rectangle.y + 2 * radius == PLAY_AREA.getPlatform()
							.getLocationPoint().y
							|| rectangle.y + 2 * radius == PLAY_AREA
									.getPlatform().getLocationPoint().y + 1 || rectangle.y
							+ 2 * radius == PLAY_AREA.getPlatform()
							.getLocationPoint().y + 2)) {
						if (((rectangle.x + 2 * radius >= PLAY_AREA
								.getPlatform().getLocationPoint().x && rectangle.x
								+ 2 * radius <= PLAY_AREA.getPlatform()
								.getLocationPoint().x + 3)
								&& vectorMove.width > 0 && vectorMove.height > 0) // uderzenie
																					// w
																					// lewy
																					// g�rny
																					// r�g
																					// platformy

								|| ((rectangle.x <= PLAY_AREA.getPlatform()
										.getLocationPoint().x
										+ PLAY_AREA.getPlatform().getWidth() && rectangle.x >= PLAY_AREA
										.getPlatform().getLocationPoint().x
										+ PLAY_AREA.getPlatform().getWidth() - 3)
										&& vectorMove.height > 0 && vectorMove.width < 0)) { // uderzenie
							// w
							// prawy
							// g�rny
							// r�g
							// platformy
							vectorMove.height = -vectorMove.height;
							vectorMove.width = -vectorMove.width;

						} else { // odbicie od boku platformy
							vectorMove.width = -vectorMove.width;
						}
					} else { // odbicie od boku platformy
						vectorMove.width = -vectorMove.width;
					}
				}
			}
		}
		if (hitedBlock() != null) {
			hited = hitedBlock();
			PLAY_AREA.getScorePanel().addPoints(PLAY_AREA.getBlocks().getLevel());
			PLAY_AREA.getBlocks().removeRectangle(hited);
			if (((rectangle.y + 2 * radius == hited.y && vectorMove.height > 0) || (rectangle.y == hited.y
					+ hited.height && vectorMove.height < 0))
					&& rectangle.x + 2 * radius >= hited.x
					&& rectangle.x <= hited.x + hited.getWidth()) { // odbicie
																	// od
																	// g�rnej/dolnej
																	// powierzchni
																	// bloku
				vectorMove.height = -vectorMove.height;
			} else {
				if (((rectangle.y + 2 * radius == hited.y || rectangle.y + 2
						* radius == hited.y + 1)
						&& (rectangle.x + 2 * radius >= hited.x && rectangle.x
								+ 2 * radius <= hited.x + 1)
						&& vectorMove.width > 0 && vectorMove.height > 0) // uderzenie
																			// w
																			// lewy
																			// g�rny
																			// r�g
																			// bloku

						|| ((rectangle.x <= hited.x + hited.width && rectangle.x >= hited.x
								+ hited.width // uderzenie w prawy g�rny r�g
								- 1)
								&& (rectangle.y + 2 * radius == hited.y || rectangle.y
										+ 2 * radius == hited.y + 1)
								&& vectorMove.height > 0 && vectorMove.width < 0)

						|| ((rectangle.y == hited.y + hited.height || rectangle.y == hited.y // uderzenie
																								// w
																								// lewy
																								// dolny
																								// r�g
								+ hited.height - 1)
								&& (rectangle.x + 2 * radius >= hited.x && rectangle.x
										+ 2 * radius <= hited.x + 1)
								&& vectorMove.width > 0 && vectorMove.height < 0)

						|| ((rectangle.x <= hited.x + hited.width && rectangle.x >= hited.x
								+ hited.width - 1)
								&& (rectangle.y == hited.y + hited.height || rectangle.y == hited.y // uderzenie
																									// w
																									// prawy
																									// dolny
																									// r�g
										+ hited.height - 1)
								&& vectorMove.width < 0 && vectorMove.height < 0)) {

					vectorMove.height = -vectorMove.height;
					vectorMove.width = -vectorMove.width;
				} else { // odbicie od boku bloku
					vectorMove.width = -vectorMove.width;
				}
			}
		}

	}

	public void render (Graphics g) {
		g.setColor(Color.red);
		g.fillOval((int) rectangle.getX(), (int) rectangle.getY(), 2 * radius,
				2 * radius);
	}

	private boolean ballHitPlatform() {	//sprawdza, czy kula uderzy�a platforme
		afterHitCounter--;
		Rectangle platform = new Rectangle(PLAY_AREA.getPlatform().getLocationPoint(), new Dimension(PLAY_AREA.getPlatform().getWidth(), PLAY_AREA.getPlatform().getHeight()));
		if (afterHitCounter == 10)
			isBallHitedByPlatform = false;
		if (platform.intersects(
						new Rectangle(rectangle.x - 1, rectangle.y - 1,
								rectangle.width + 2, rectangle.height + 2))) {
			if (afterHitCounter <= 0) {
				isBallHitedByPlatform = true;
				afterHitCounter = 20;
				return true;
			}
		}
		return false;
	}

	public static boolean isHitedByPlatform() {
		return isBallHitedByPlatform;
	}

	protected Rectangle hitedBlock() {
		for (Rectangle blocksRectangle : PLAY_AREA.getBlocks()
				.getListOfRectangle()) {
			if (blocksRectangle
					.intersects(new Rectangle(rectangle.x - 1, rectangle.y - 1,
							rectangle.width + 2, rectangle.height + 2))) {
				afterHitCounter = 10;
				return blocksRectangle;
			}
		}
		return null;
	}

	public Point getLocationPoint() {
		return rectangle.getLocation();
	}

	public int getRadius() {
		return radius;
	}

	public void setAfterHitCounter(int i) {
		afterHitCounter = i;
		isBallHitedByPlatform = true;
	}
}

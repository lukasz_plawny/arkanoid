import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

@SuppressWarnings("serial")
public final class PlayArea extends JPanel {
	private Platform platform;
	private boolean platformBlock = false;
	private Ball ball;
	private AtomicBoolean isGameRunning = new AtomicBoolean(false),
			isGamePaused = new AtomicBoolean(false),
			firstMove = new AtomicBoolean(true); // czy kula dopiero zacznie
													// ruch
	private final Object PAUSED = new Object();
	private Blocks blocks;
	private final JFrame FRAME;
	private ScorePanel scorePanel;
	private final Image BACKGROUND = new ImageIcon(getClass().getResource(
			"/drawable/background.jpg")).getImage();
	private static final String START = "press enter to start";
	private static final String PAUSE = "pause, \n press enter or space to resume";
	private String toComunicate = "", toComunicateSecondLine = "";
	private static final Font COMUNICATE_FONT = new Font(Font.SANS_SERIF,
			Font.BOLD, Main.getFrameHeight() / 15);
	private final Thread GAME_THREAD = new Thread(new Runnable() {

		@Override
		public void run() {
			isGameRunning.set(true);
			ball.setVectorMove(1, -1);
			while (isGameRunning.get()) {
				if (blocks.getNumberOfRectangle() != 0) {
					ball.moveBall();
					repaint();
					try {
						Thread.sleep(8);
						synchronized (PAUSED) {
							while (isGamePaused.get()) {
								PAUSED.wait();
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					levelUp();
				}
			}
		}
	});

	public PlayArea(JFrame frame) {
		this.FRAME = frame;
		scorePanel = new ScorePanel();
		FRAME.add(scorePanel, BorderLayout.NORTH);
		InputMap inputMap = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap actionMap = getActionMap();
		inputMap.put(KeyStroke.getKeyStroke("LEFT"), "moveLeft");
		inputMap.put(KeyStroke.getKeyStroke("RIGHT"), "moveRight");
		inputMap.put(KeyStroke.getKeyStroke("ENTER"), "startGame");
		inputMap.put(KeyStroke.getKeyStroke("SPACE"), "pauseGame");
		actionMap.put("moveLeft", new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!platformBlock) {
					platform.horizontalMove(-15);
					repaint();
				}
			}
		});
		actionMap.put("moveRight", new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!platformBlock) {
					platform.horizontalMove(15);
					repaint();
				}
			}
		});
		actionMap.put("startGame", new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!isGameRunning.get()) {
					toComunicateSecondLine = toComunicate = "";
					platformBlock = false;
					hitBallByPlatform();
					firstMove.set(false);
					GAME_THREAD.start();
				} else {
					synchronized (PAUSED) { // wznowienie gry
						toComunicateSecondLine = toComunicate = "";
						if (firstMove.get()) {
							hitBallByPlatform();
							firstMove.set(false);
						}
						isGamePaused.set(false);
						platformBlock = false;
						PAUSED.notify();
					}
				}
			}
		});
		actionMap.put("pauseGame", new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!isGamePaused.get()) {
					toComunicate = PAUSE;
					isGamePaused.set(true);
					platformBlock = true;
				} else { // wznowienie gry
					synchronized (PAUSED) {
						toComunicateSecondLine = toComunicate = "";
						if (firstMove.get()) {
							hitBallByPlatform();
							firstMove.set(false);
						}
						isGamePaused.set(false);
						platformBlock = false;
						PAUSED.notify();
					}
				}

			}
		});
		setLayout(new GridLayout());
		ball = new Ball(this);
		blocks = new Blocks();
		blocks.makeRectangle();
		platform = new Platform(this);
		platformBlock = true;
		toComunicate = START;

	}

	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(BACKGROUND, 0, 0, getWidth(), getHeight(), null);
		platform.render(g);
		ball.render(g);
		blocks.render(g);
		g.setColor(Color.WHITE);
		g.setFont(COMUNICATE_FONT);
		g.drawString(
				toComunicate,
				(int) (getWidth() - COMUNICATE_FONT
						.getStringBounds(toComunicate,
								g.getFontMetrics().getFontRenderContext())
						.getWidth()) / 2, getHeight() / 2);
		g.drawString(toComunicateSecondLine, (int) (getWidth() - COMUNICATE_FONT
						.getStringBounds(toComunicateSecondLine,
								g.getFontMetrics().getFontRenderContext())
						.getWidth()) / 2, getHeight() / 2 + g.getFontMetrics().getHeight());

	}

	private void resetBeforeNextLevel() {
		isGamePaused.set(true);
		platformBlock = true;
		ball.reset();
		platform.reset();
		firstMove.set(true);
		ball.setVectorMove(1, -1);
	}

	protected void endGame() {
		toComunicate = "GAME OVER your score is: "
				+ scorePanel.getScore();
		toComunicateSecondLine = "press enter to play again";
		resetBeforeNextLevel();
		blocks.reset();
		scorePanel.resetScore();
	}

	private void winGame() {
		toComunicate = "YOU WON your score is: "
				+ scorePanel.getScore();
		toComunicateSecondLine = "press enter to play again";
		resetBeforeNextLevel();
		blocks.reset();
		scorePanel.resetScore();
	}

	private void levelUp() {
		blocks.nextLevel();
		if (blocks.getLevel() == 21) {
			winGame();
		} else {
			blocks.makeRectangle();
			toComunicate = "LEVEL " + blocks.getLevel();
			toComunicateSecondLine = "press enter to start";
			resetBeforeNextLevel();
			repaint();
			try {
				synchronized (PAUSED) {
					while (isGamePaused.get())
						PAUSED.wait();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void hitBallByPlatform() { // rozpocznij gre - uderz kule platform�
		ball.setAfterHitCounter(20);
		platform.verticalMove(ball.getLocationPoint().y + 2 * ball.getRadius()
				- platform.getLocationPoint().y);
	}

	protected Platform getPlatform() {
		return platform;
	}

	protected Blocks getBlocks() {
		return blocks;
	}

	protected Ball getBall() {
		return ball;
	}

	protected ScorePanel getScorePanel() {
		return scorePanel;
	}
}

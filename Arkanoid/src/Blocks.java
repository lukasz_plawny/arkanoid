import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Lukasz Plawny consist and draws rectangles,which will be destroyed
 *         during game by ball hitting
 */
public class Blocks {
	private int level = 1;
	private List<Rectangle> rectangleList = new ArrayList<Rectangle>();

	/**
	 * make new rectangles, numbers of them depends of game's level
	 */
	protected void makeRectangle() {
		int y = Main.getFrameHeight() * 1 / 5;
		for (int i = 0; i < 2 * level; i++) {
			if (rectangleList.size() % 10 == 0) { // zape�niony rz�d,
													// przechodzimy do kolejnego

				if ((rectangleList.size() / 10) % 2 != 0)
					y -= rectangleList.size() / 10 * Main.getFrameHeight() * 1
							/ 20;
				else
					y += rectangleList.size() / 10 * Main.getFrameHeight() * 1
							/ 20;
			}
			rectangleList.add(new Rectangle(Main.getFrameWidth() * (5 + i % 5)
					/ 10, y, Main.getFrameWidth() / 10,
					Main.getFrameHeight() / 20));
			rectangleList.add(new Rectangle(Main.getFrameWidth() * (4 - i % 5)
					/ 10, y, Main.getFrameWidth() / 10,
					Main.getFrameHeight() / 20));
		}
	}

	protected void nextLevel() {
		level++;
	}

	public void render(Graphics g) {
		for (Rectangle rectangle : rectangleList) {
			g.setColor(Color.blue);
			g.fillRect(rectangle.x, rectangle.y, rectangle.width,
					rectangle.height);
			g.setColor(Color.black);
			g.drawRect(rectangle.x, rectangle.y, rectangle.width,
					rectangle.height);
		}
	}

	/**
	 * @return current number of rectangles to destroy
	 */
	public int getNumberOfRectangle() {
		return rectangleList.size();
	}

	/**
	 * @return current level of the game
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * set level to 1 and remake rectangles
	 */
	protected void reset() {
		level = 1;
		rectangleList.clear();
		makeRectangle();
	}

	/**
	 * @param hited
	 *            rectangle to remove
	 */
	protected void removeRectangle(Rectangle hited) {
		rectangleList.remove(hited);
	}

	protected List<Rectangle> getListOfRectangle() {
		return rectangleList;
	}
}

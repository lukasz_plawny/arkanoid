import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * @author Lukasz Plawny
 *	A <code>ScorePanel</code> class is a JPanel, which contains and shows number of points
 */
@SuppressWarnings("serial")
public class ScorePanel extends JPanel {
	private final static JLabel SCORE_DESCRIPTION;
	private JLabel score;
	
	static {
		SCORE_DESCRIPTION = new JLabel("score:",
				SwingConstants.CENTER);
		SCORE_DESCRIPTION.setFont(new Font(Font.SANS_SERIF, Font.BOLD, Main
				.getFrameHeight() / 20));
	}

	public ScorePanel() {
		score = new JLabel("0", SwingConstants.CENTER);
		score.setFont(new Font(Font.SANS_SERIF, Font.BOLD, Main
				.getFrameHeight() / 20));
		setLayout(new GridLayout(1, 2));
		add(SCORE_DESCRIPTION);
		add(score);
		setBorder(BorderFactory.createLineBorder(Color.black));
	}
	/**
	 * @param points number of points to be added
	 */
	public void addPoints(int points) {
		if (points > 0)
			score.setText(Integer.toString(getScore() + points));
	}
	/**
	 * @return current number of points
	 */
	public int getScore() {
		return Integer.parseInt(score.getText());
	}
	/**
	 * set score to zero
	 */
	public void resetScore() {
		score.setText("0");
	}
	@Override
	public String toString (){
		return getClass().getName() + "[score = " + score.getText() + "]";
	}
}

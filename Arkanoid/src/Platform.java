import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JComponent;

public class Platform {
	private Rectangle rectanglePlatform = new Rectangle(); // po�o�enie i wymiar
															// platformy
	private boolean firstPaint = true;
	private final PlayArea PLAY_AREA;

	public Platform(PlayArea playArea) {
		this.PLAY_AREA = playArea;
	}

	public void reset() { // przywr�� platforme do pierwotnego po�o�enia
		rectanglePlatform.width = PLAY_AREA.getWidth() / 8;
		rectanglePlatform.height = PLAY_AREA.getHeight() / 30;
		rectanglePlatform.x = PLAY_AREA.getWidth() / 2
				- rectanglePlatform.width / 2;
		rectanglePlatform.y = PLAY_AREA.getHeight() * 8 / 9;

	}
	
	public void render (Graphics g) {
		if (firstPaint) {
			reset();
			firstPaint = false;
		}
		if (Ball.isHitedByPlatform())
			g.setColor(Color.red);
		else
			g.setColor(Color.white);
		g.fillRect(rectanglePlatform.x, rectanglePlatform.y,
				rectanglePlatform.width, rectanglePlatform.height);
	}

	/**
	 * moves platform horizontal
	 * 
	 * @param x
	 *            number of pixels, which plaform moves horizontal. If x is
	 *            positive - platform move right. If x is negative - platform
	 *            move left
	 */
	public void horizontalMove(int x) {
		rectanglePlatform.x += x;
		if (rectanglePlatform.x < 0)
			rectanglePlatform.x = 0;
		else if (rectanglePlatform.x > PLAY_AREA.getWidth()
				- rectanglePlatform.width)
			rectanglePlatform.x = PLAY_AREA.getWidth()
					- rectanglePlatform.width;
	}

	public void verticalMove(int y) {
		rectanglePlatform.y += y;
		if (rectanglePlatform.y < PLAY_AREA.getY())
			rectanglePlatform.y = PLAY_AREA.getY();
		else if (rectanglePlatform.y > PLAY_AREA.getHeight()
				- rectanglePlatform.height)
			rectanglePlatform.y = PLAY_AREA.getHeight()
					- rectanglePlatform.height;
	}

	public Point getLocationPoint() {
		return rectanglePlatform.getLocation();
	}

	public int getWidth() {
		return rectanglePlatform.width;
	}

	public int getHeight() {
		return rectanglePlatform.height;
	}
}